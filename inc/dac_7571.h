#ifndef _24LC128_H_
#define _24LC128_H_

/*==================[inclusions]=============================================*/

#include <stdint.h> /* int16_t, uint8_t, etc. */

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @brief i2c bus and memory initialization
 */
void DacInit(void);


/** @brief i2c memory write function
 *
 * @param id		slave id
 * @param addr		memory address
 * @param data		user data buffer
 * @param datalen	data length
 * @return number of bytes written (max @ref datalen)
 */
int16_t DacWrite(uint8_t id, void * data, uint16_t datalen);

/*==================[end of file]============================================*/
#endif /* #ifndef _24LC128_H_ */
