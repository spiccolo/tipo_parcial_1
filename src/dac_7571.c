/*==================[inclusions]=============================================*/

#include "../inc/dac_7571.h"

#include "board.h"

/*==================[macros and definitions]=================================*/

#define I2C_PORT I2C0
#define PAGESIZE 32

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
void DacInit(void)
{
	Board_I2C_Init(I2C_PORT);
	Chip_I2C_SetClockRate(I2C_PORT, 100000);
	Chip_I2C_SetMasterEventHandler(I2C_PORT, Chip_I2C_EventHandlerPolling);
}


int16_t DacWrite(uint8_t id,  void* data, uint16_t datalen)
{
	uint8_t buff1 [2];					// pagina donde se guardan los datos a enviar
	I2C_XFER_T xfer;
	int sw=0;									// flag status de la escritura

	buff1[0]=*(uint8_t *)data>>8;						//la parte alta de la direccion de memoria va en wbuff[0]
	buff1[0]=buff1[0]&0x0F;
	buff1[1]=*(uint8_t *)data&0xFF;						//la parte baja de la direccion de memoria va en wbuff[1]

	xfer.rxBuff = 0;
	xfer.rxSz = 0;
	xfer.slaveAddr = id;
	xfer.status = 0;
	xfer.txBuff = buff1;
	xfer.txSz = 2;

	sw=Chip_I2C_MasterTransfer(I2C_PORT, &xfer);

	if (sw!=0)									//si sw es distinto de I2C_STATUS_DONE devuelve -1, sino devuelve el numero de bytes enviados
		return -1;
	else
		return sw;
}
