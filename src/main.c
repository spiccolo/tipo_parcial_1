/* Copyright 2016, Pablo Ridolfi
 * All rights reserved.
 *
 * This file is part of Workspace.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Este es el tp3*/

/*==================[inclusions]=============================================*/

#include "main.h"

#include "../inc/dac_7571.h"
#include "Tipo_parcial_1.h"
#include "board.h"

/* Include statechart header file. Be sure you run the statechart C code
 * generation tool!
 */

/*==================[macros and definitions]=================================*/

//#define LED3_PORT  1 	/* LED3: P2_12 pin -> GPIO1[12] @ FUNC0 */
//#define LED3_PIN   12

#define EXT_TEC_PORT	3 /* external button ->GPIO3[0] @ FUNC0 */
#define EXT_TEC_PIN		0


/*==================[internal data declaration]==============================*/

/** statechart instance */
static Tipo_parcial_1 statechart;
bool adc_ready=false;
static uint16_t adc_value[5];
static uint32_t prom,i,buttonPressed;

/*==================[internal functions declaration]=========================*/

/** @brief hardware initialization function
 *	@return none
 */
static void initHardware(void);

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/** clock and peripherals initialization */
static void initHardware(void)
{
	Board_Init();
	SystemCoreClockUpdate();

	// external button configuration P6_1
	Chip_SCU_PinMux(6, 1, SCU_MODE_PULLUP | SCU_MODE_INBUFF_EN,  SCU_MODE_FUNC0 );

	// set button as input
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT,EXT_TEC_PORT,EXT_TEC_PIN);

	// Set it as the source for pin interrupt 0 (8 available) [UM:17.4.11]
	Chip_SCU_GPIOIntPinSel(0, EXT_TEC_PORT,EXT_TEC_PIN);

	// Configure the interrupt 0 as falling edge [UM:19.5.1.1] [UM:19.5.1.6]
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH0);
	Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT, PININTCH0);

	ADC_CLOCK_SETUP_T adc_setup;

	/* EDU-CIAA ADC0 configuration */
	// ADC0_1/ADC1_1 pin -> transparent analog pad [UM:Table 187]

	// Setup ADC0 with the default values: 10-bit, 400kSPS
	Chip_ADC_Init(LPC_ADC0, &adc_setup);  // [UM:47.6.1]

	// Enable ADC0 CH1 and its interrupt
	Chip_ADC_EnableChannel(LPC_ADC0, ADC_CH1, ENABLE);      // [UM:47.6.1]
	Chip_ADC_Int_SetChannelCmd(LPC_ADC0, ADC_CH1, ENABLE);  // [UM:47.6.3]

	// Enable ADC0 CH1 and its interrupt
	Chip_ADC_EnableChannel(LPC_ADC0, ADC_CH2, ENABLE);      // [UM:47.6.1]
	Chip_ADC_Int_SetChannelCmd(LPC_ADC0, ADC_CH2, ENABLE);  // [UM:47.6.3]

	// Enable ADC0 CH1 and its interrupt
	Chip_ADC_EnableChannel(LPC_ADC0, ADC_CH3, ENABLE);      // [UM:47.6.1]
	Chip_ADC_Int_SetChannelCmd(LPC_ADC0, ADC_CH3, ENABLE);  // [UM:47.6.3]

	// Enable ADC0 CH1 and its interrupt
	Chip_ADC_EnableChannel(LPC_ADC0, ADC_CH4, ENABLE);      // [UM:47.6.1]
	Chip_ADC_Int_SetChannelCmd(LPC_ADC0, ADC_CH4, ENABLE);  // [UM:47.6.3]

	// Enable ADC0 CH1 and its interrupt
	Chip_ADC_EnableChannel(LPC_ADC0, ADC_CH5, ENABLE);      // [UM:47.6.1]
	Chip_ADC_Int_SetChannelCmd(LPC_ADC0, ADC_CH5, ENABLE);  // [UM:47.6.3]

	// Enable ADC0 interrupt in the NVIC [UM:9.7]
	NVIC_ClearPendingIRQ(ADC0_IRQn);
	NVIC_EnableIRQ(ADC0_IRQn);

	/////////////////////////////////////////////////////////////////////////////////////////////

	/* Timer */
	Chip_TIMER_Init(LPC_TIMER1);
	Chip_TIMER_PrescaleSet(LPC_TIMER1, Chip_Clock_GetRate(CLK_MX_TIMER1) / 1000 - 1 );

	/* Match 0 (debounce) */
	Chip_TIMER_MatchEnableInt(LPC_TIMER1, 0);
	Chip_TIMER_ResetOnMatchEnable(LPC_TIMER1, 0);
	Chip_TIMER_StopOnMatchDisable(LPC_TIMER1, 0);
	Chip_TIMER_SetMatch(LPC_TIMER1, 0, 25);

	/* Match 1 (DAC) */
	Chip_TIMER_MatchEnableInt(LPC_TIMER1, 1);
	Chip_TIMER_ResetOnMatchDisable(LPC_TIMER1, 1);
	Chip_TIMER_StopOnMatchDisable(LPC_TIMER1, 1);
	Chip_TIMER_SetMatch(LPC_TIMER1, 1, 1000);

	Chip_TIMER_Reset(LPC_TIMER1);
	Chip_TIMER_Enable(LPC_TIMER1);



	// Enable the timer interrupt
	NVIC_EnableIRQ(TIMER1_IRQn);

	// Enable the interrupt 0 in the NVIC [UM:9.7]
	NVIC_ClearPendingIRQ(PIN_INT0_IRQn);
	NVIC_EnableIRQ(PIN_INT0_IRQn);

	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	//EDU-CIAA LED3 (P2_12 pin) configuration
	//	Chip_SCU_PinMux(2, 12, SCU_MODE_PULLUP, SCU_MODE_FUNC0);
	//	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LED3_PORT, LED3_PIN);
	//	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, LED3_PORT, LED3_PIN);

	DacInit();

}

/*==================[external functions definition]==========================*/
/* ADC0 interrupt handler triggered when the A/D conversion completes */
void ADC0_IRQHandler(void)
{
	adc_ready=1;
}

/* EXTERN TEC button is pressed (falling edge) raise evbutton */
void GPIO0_IRQHandler(void)
{
	/* if a button is pressed, send evButton to state machine */
	tipo_parcial_1Iface_raise_evButton(&statechart);

	// Clear pending interrupts [UM:19.5.1.10]
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH0);

}



void tipo_parcial_1Iface_opREAD(const Tipo_parcial_1* handle)
{
	prom=0;

	Chip_ADC_SetStartMode(LPC_ADC0, ADC_START_NOW, 0);

	if(adc_ready){
		if (Chip_ADC_ReadStatus(LPC_ADC0, ADC_CH1, ADC_DR_DONE_STAT) == SET)
			Chip_ADC_ReadValue(LPC_ADC0, ADC_CH1, &adc_value[0]);  // [UM:47.6.4]

		if (Chip_ADC_ReadStatus(LPC_ADC0, ADC_CH2, ADC_DR_DONE_STAT) == SET)
			Chip_ADC_ReadValue(LPC_ADC0, ADC_CH2, &adc_value[1]);  // [UM:47.6.4]

		if (Chip_ADC_ReadStatus(LPC_ADC0, ADC_CH3, ADC_DR_DONE_STAT) == SET)
			Chip_ADC_ReadValue(LPC_ADC0, ADC_CH3, &adc_value[2]);  // [UM:47.6.4]

		if (Chip_ADC_ReadStatus(LPC_ADC0, ADC_CH4, ADC_DR_DONE_STAT) == SET)
			Chip_ADC_ReadValue(LPC_ADC0, ADC_CH4, &adc_value[3]);  // [UM:47.6.4]

		if (Chip_ADC_ReadStatus(LPC_ADC0, ADC_CH5, ADC_DR_DONE_STAT) == SET)
			Chip_ADC_ReadValue(LPC_ADC0, ADC_CH5, &adc_value[4]);  // [UM:47.6.4]

		adc_ready=0;

		for(i=0; i<5; i++)
			prom=prom+adc_value[i];

		prom=prom/5;

	}
}
void TIMER1_IRQHandler(void)
{
	if (Chip_TIMER_MatchPending(LPC_TIMER1, 0)) {
		Chip_TIMER_ClearMatch(LPC_TIMER1, 0);
		if ((Chip_GPIO_GetPinState(LPC_GPIO_PORT, EXT_TEC_PORT, EXT_TEC_PIN)== NO_BUTTON_PRESSED))
			/* use a flag to wait for button release */
			buttonPressed = 1;
		else
			buttonPressed = 0;
		/*send evTick and buttonPressed to statechart */
		tipo_parcial_1Iface_raise_evTickDebounce(&statechart);
		tipo_parcial_1Iface_set_buttonStatus(&statechart, buttonPressed);
	}

	if (Chip_TIMER_MatchPending(LPC_TIMER1, 1)) {
		Chip_TIMER_ClearMatch(LPC_TIMER1, 1);
		tipo_parcial_1Iface_raise_evTickDAC(&statechart);
	}
}

void tipo_parcial_1Iface_opSEND(const Tipo_parcial_1* handle)
{
	DacWrite(0x98, &prom, 1);
	Chip_TIMER_Reset(LPC_TIMER1);
}

/** main function, application entry point */
int main(void)
{

	initHardware();

	/** init and reset state machine */
	tipo_parcial_1_init(&statechart);
	tipo_parcial_1_enter(&statechart);

	while (1) {
		__WFI();
		/* update state machine */
		tipo_parcial_1_runCycle(&statechart);
	}

}
/** @} doxygen end group definition */

/*==================[end of file]============================================*/
